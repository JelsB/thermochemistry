from chemical_species import Species
from utils import all_subclasses, cleaned_to_cgs
from typing import List, Dict, Union, Callable
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
from astropy.constants import k_B, N_A


class Reaction(object):
    """docstring for Reaction."""

    def __init__(self, reactants: List[Species], products: List[Species],
                 rate_parameters: Dict[str, Union[float, Callable]]) -> None:
        self.reactants = reactants
        self.products = products
        self.rate_parameters = rate_parameters

    def __repr__(self):
        reactant_names = [r.name for r in self.reactants]
        product_names = [p.name for p in self.products]
        return f'{" + ".join(reactant_names)} -> {" + ".join(product_names)}'

    def plot_rate(self, x):
        fig, ax = plt.subplots()
        y = self.rate(x)
        ax.semilogy(x, y)
        plt.show()

    @classmethod
    def subclass_dict(cls):
        d = {i.__name__: i for i in all_subclasses(cls)}
        d.update({cls.__name__: cls})

        return d

    @classmethod
    def from_dict(cls, input_dict, species):
        rate_params = dict()
        output_dict = dict()
        input_rate_params = input_dict['rate']['params']
        type = input_dict['rate']['type']
        subclasses = cls.subclass_dict()
        reaction_class = subclasses[type]

        for spec_types in ['reactants', 'products']:
            output_dict[spec_types] = []
            for sp in input_dict[spec_types]:
                output_dict[spec_types].append(species[sp])

        if input_rate_params:
            for key, item in input_rate_params.items():
                value = item['value']
                unit = u.Unit(item['unit'])
                if isinstance(value, str):
                    def rate(T): return eval(value) * unit
                else:
                    rate = value * unit

                rate_params[key] = rate

        output_dict['rate_parameters'] = rate_params
        # del input_dict['rate']
        # print(input_dict)
        return reaction_class(**output_dict)


class Arrhenius(Reaction):
    """docstring for Arrhenius."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @cleaned_to_cgs
    def rate(self, temperature):
        """form: A*exp(-E/T)"""
        A = self.rate_parameters['alpha']
        E = self.rate_parameters['activation_energy']

        return A * np.exp(-E / temperature)


class ModifiedArrhenius(Arrhenius):
    """docstring for ModifiedArrhenius."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @cleaned_to_cgs
    def rate(self, temperature):
        """form: A*T^b*exp(-E/T)"""
        Arrhenius_rate = super().rate(temperature)
        b = self.rate_parameters['beta']

        return temperature**b * Arrhenius_rate


class Lindemann(Reaction):
    """docstring for Lindemann."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @cleaned_to_cgs
    def rate(self, temperature, needed_for_Troe=False):
        """form: k0*ntot/(1 + k0*ntot/kinf)"""
        rate_low_density = self.rate_parameters['rate_low_density']
        rate_high_density = self.rate_parameters['rate_high_density']
        catalyst_number_density = self.rate_parameters['catalyst_number_density']

        a = rate_low_density(temperature) * catalyst_number_density
        b = a / rate_high_density(temperature)
        rate = a / (1 * b.unit + b)
        if not needed_for_Troe:
            return rate
        return rate, b


class Troe(Lindemann):
    """docstring for Troe."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @cleaned_to_cgs
    def rate(self, temperature):
        """form: k0*ntot/(1 + k0*ntot/kinf)*F**x"""
        rate, b = super().rate(temperature, needed_for_Troe=True)
        falloff_exponent = 1 / (1 + (np.log10(b)**2))
        falloff_function = self.rate_parameters['falloff_function']

        return rate * falloff_function(temperature)**falloff_exponent


class RigidSpheres(Reaction):
    """docstring for RigidSpheres."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @cleaned_to_cgs
    def rate(self, temperature):
        sum_of_radii = sum([r.radius for r in self.reactants])
        reduced_mass = 1 / sum([1 / r.mass for r in self.reactants])

        return(np.pi * sum_of_radii**2 *
               np.sqrt(8 * k_B * temperature / (np.pi * reduced_mass)))
