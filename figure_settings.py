import matplotlib.pyplot as plt

font = {'size': 20}
text = {'usetex': True}
lines = {'linewidth': 8, 'markersize': 8, 'markeredgewidth': 1}
axes = {'xmargin': 0}
#markers = {'fillstyle': 'none'}
savefig = {'dpi': 220, 'format': 'png', 'transparent': True, 'bbox': 'tight'}
# savefig = {'format': 'pdf', 'transparent': True, 'bbox': 'tight'}
#figure = {'figsize': (16, 27)}
figure = {'figsize': (7, 5)}
legend = {'markerscale': 1.5, 'numpoints': 1,
          'fontsize': 'xx-small', 'frameon': False, 'handlelength': 2.7}
xtick = {'top': True}
xtick_major = {'size': 10, 'width': 1.5}
xtick_minor = {'size': 5, 'width': 1.5, 'visible': True}
ytick = {'right': True}
ytick_major = {'size': 10, 'width': 1.5}
ytick_minor = {'size': 5, 'width': 1.5, 'visible': True}
plt.rc('font', **font)
# plt.rc('markers',**markers)
plt.rc('text', **text)
plt.rc('lines', **lines)
plt.rc('savefig', **savefig)
plt.rc('figure', **figure)
plt.rc('legend', **legend)
plt.rc('axes', **axes)
plt.rc('xtick', **xtick)
plt.rc('ytick', **ytick)
plt.rc('xtick.major', **xtick_major)
plt.rc('xtick.minor', **xtick_minor)
plt.rc('ytick.major', **ytick_major)
plt.rc('ytick.minor', **ytick_minor)
