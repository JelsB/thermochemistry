# module with atomic data
from astropy.constants import m_e, m_p, m_n
import astropy.units as u
from collections import Counter
import re


menp = m_e + m_p + m_n

# mass dictionary
atom_mass = {
    'H': m_e + m_p,
    'D': menp,
    'He': 2. * (menp),
    'Li': 3. * (m_e + m_p) + 4. * m_n,
    'Be': 4. * (m_e + m_p) + 5. * m_n,
    'C': 6. * (menp),
    'N': 7. * (menp),
    'O': 8. * (menp),
    'F': 9. * (menp) + m_n,
    'Ne': 10. * (menp),
    'Mg': 12. * (menp),
    'Na': (m_e + m_p) * 11 + m_n * 12,
    'Al': (m_e + m_p) * 13 + m_n * 14,
    'Si': 14. * (menp),
    'P': 15. * (menp) + m_n,
    'S': (menp) * 16,
    'Cl': (menp) * 17 + m_n,
    'Ti': (menp) * 22 + 6 * m_n,
    'Fe': (m_e + m_p) * 26 + m_n * 29,
    'E': 0.,
    '-': m_e,
    '+': -m_e
}


def get_species_mass(atom_counter):

    mass = 0
    for key, cnt in atom_counter.items():
        mass += cnt * atom_mass[key]

    return mass


def count_constituent_atoms(name):
    counter = Counter()
    atom_list = re.sub(r'([A-Z])', r' \1', name).split()
    for atm in atom_list:
        key, value = re.search(r'([A-Z]+[a-z]*)(\d*)', atm).group(1, 2)
        if not value:
            value = 1
        counter[key] = int(value)
    return counter


def is_atom(name):
    if len(re.findall(r'([A-Z^\d])', name)) == 1:
        return True
    else:
        return False
