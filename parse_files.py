import re
import pandas as pd
from atomic_info import is_atom
from pathlib import Path


def space_to_comma(string):
    return re.sub(r'\s+', ',', string)


def clean_input_string(string, remove_key):
    return space_to_comma(re.sub(r'.*' + remove_key + r'\D*', '', string)
                          .strip()).split(',')


def clean_enery_input_string(row):
    return row.split('=')[1].strip().split(' ')[0]


def rename_dataframe_header(dataframe, new_header):
    input_header = list(dataframe)
    rename_header = {old: new for (old, new) in zip(input_header, new_header)}
    return dataframe.rename(columns=rename_header, inplace=True)


def parse_gaussian_data(source_iter):
    # search keys for gaussian output files (*.dft)
    rot_symmetry_key = 'Rotational symmetry number'
    vib_frequencies_key = 'Frequencies --'
    rot_temperatures_key = 'Rotational temperature'
    elec_degeneracy_key = 'Multiplicity = '
    molecule_mass_key = 'Molecular mass'
    energy_key = "SCF Done:"
    zero_point_energy_key = "Zero-point correction"
    # reference_temperature = "0"

    rot_symmetry = []
    vib_frequencies = []
    rot_temperatures = []
    elec_degeneracy = []
    molecule_mass = []
    energy = []

    for row in source_iter:
        if energy_key in row and not energy:
            energy = [clean_enery_input_string(row)]

        elif rot_symmetry_key in row:
            rot_symmetry = clean_input_string(row, rot_symmetry_key)

        elif vib_frequencies_key in row:
            cleaned_row = clean_input_string(row, vib_frequencies_key)
            for i in cleaned_row:
                vib_frequencies.append(i)

        elif rot_temperatures_key in row:
            rot_temperatures = clean_input_string(
                row, rot_temperatures_key)

        elif elec_degeneracy_key in row:
            if not elec_degeneracy:
                elec_degeneracy = clean_input_string(
                    row, elec_degeneracy_key)

        elif molecule_mass_key in row:
            molecule_mass = [clean_input_string(row, molecule_mass_key)[0]]

    yield energy
    yield rot_symmetry
    yield vib_frequencies
    yield rot_temperatures
    yield elec_degeneracy
    yield molecule_mass


def get_species_data(input_file: Path):
    df = pd.read_csv(input_file)
    new_header = ['energy', 'vib_freq', 'rot_freq', 'rot_symm',
                  'elec_freq', 'elec_degen']
    rename_dataframe_header(df, new_header)

    energy = df['energy'].dropna().values
    vib_frequencies = df['vib_freq'].dropna().values
    rot_frequencies = df['rot_freq'].dropna().values
    rot_symmetry = df['rot_symm'].dropna().values
    elec_frequencies = df['elec_freq'].dropna().values
    elec_degeneracy = df['elec_degen'].dropna().values

    return (energy, vib_frequencies, rot_frequencies, rot_symmetry,
            elec_frequencies, elec_degeneracy)


def get_internal_partition_function(input_file: Path):
    internal_partition = pd.read_csv(input_file)

    new_header = ['temperature', 'internal_partition']
    rename_dataframe_header(internal_partition, new_header)
    return internal_partition


def get_thermochem_data(input_file: Path, species_name):
    df = pd.read_csv(input_file)
    new_header = ['temp', 'entropy', 'diff_enthalpy', 'formation_enthalpy',
                  'energy', 'E_zpve']
    rename_dataframe_header(df, new_header)

    if is_atom(species_name):
        diff_enthalpy = df['diff_enthalpy'].values[0]
        energy = df['energy'].values[0]
        return diff_enthalpy, energy

    else:
        temperature = df['temp'].values
        entropy = df['entropy'].values
        diff_enthalpy = df['diff_enthalpy'].values
        formation_enthalpy_0 = df.loc[df['temp'] == 0]['formation_enthalpy'].values[0]
        formation_enthalpy_298 = df.loc[df['temp'] == 298.15]['formation_enthalpy'].values[0]
        energy = df['energy'].values[0]
        E_zpve = df['E_zpve'].values[0]
        return (temperature, entropy, diff_enthalpy, formation_enthalpy_0,
                formation_enthalpy_298, energy, E_zpve)
