import numpy as np
import pandas as pd
import astropy.units as u
from astropy.constants import h, k_B, R, N_A
from scipy.interpolate import griddata
from atomic_info import get_species_mass, count_constituent_atoms, is_atom


pi = 3.1415
Hartree = (2. * u.Ry).to(u.J)


class Species:
    """
    Define a chemical species.

    This can be any type of molecule or an atom.
    """

    def __init__(self, name, mass, energy, elec_energies, elec_degeneracies,
                 internal_partition_df: pd.DataFrame):
        "Initialise the species object"
        self.name = name
        self.mass = self.set_mass(mass)
        #  energy per particle in Joule
        self.energy = self.get_energy(energy)
        self.elec_energies = elec_energies
        self.elec_degeneracies = elec_degeneracies
        self.internal_partition_df = internal_partition_df

    def __repr__(self):
        return "{class_}({name})".format(
            class_=self.__class__.__name__, **vars(self))

    def convert_to_dict(self, skip_attrs=None):
        """Convert obj to dict with optional skipped attributes."""
        dict = vars(self)
        dict['internal_partition_df'] = dict['internal_partition_df'].to_dict('list')
        if isinstance(skip_attrs, str):
            del dict[skip_attrs]
        elif isinstance(skip_attrs, list):
            for key in skip_attrs:
                del dict[key]

        return dict

    @classmethod
    def from_dict(cls, input_dict):
        """Create object from dictionary."""
        new_dict = dict()
        # print(dict)
        for key, item in input_dict.items():
            if key == 'internal_partition_df':
                item = pd.DataFrame.from_dict(item)
            elif isinstance(item, dict):
                values = item['value']
                unit = u.Unit(item['unit'])
                item = values * unit

            new_dict[key] = item

        return cls(**new_dict)
        # print(dict)
        # return cls(**dict)

    def set_mass(self, mass):
        if isinstance(mass, u.Quantity):
            return mass
        elif mass:
            return mass.to(u.kg)
        else:
            return get_species_mass(count_constituent_atoms(self.name))

    @staticmethod
    def get_energy(energy):
        if isinstance(energy, u.Quantity):
            return energy
        else:
            return energy * Hartree

    def translational_partition(self, Tgas, pressure=1 * u.bar):
        """Calculate translational partition function.

        Default is at standard pressure of 1 bar (1e5 Pa)
        """
        # decompose resolves the units make them cancel
        return ((2. * self.mass * pi * k_B * Tgas / h**2)**(3. / 2.)
                * k_B * Tgas * N_A.value / pressure.to(u.Pa)).decompose()

    def electronic_partition(self, Tgas):
        """Calculate electronic energy.

        Z_e = SUM_0^N g_i exp(-E_i/(k_B*Tgas)) becomes
        Under the assumption that the Nth electronic excitation energy
        is much greater than k_B*Tgas.
        All energy levels are w.r.t. the lowest one, so E_0=0.
        """
        result = 0
        for g, E in zip(self.elec_degeneracies, self.elec_energies):
            result += g * np.exp(-E / (k_B * Tgas))

        return result

    def internal_partition(self, Tgas):
        "Interpolate internal partition function from given dataframe."
        Tgas_partition_function = self.internal_partition_df['temperature'].values
        try:
            if (max(Tgas).value > max(Tgas_partition_function)
                    or min(Tgas).value < min(Tgas_partition_function)):
                raise ValueError('Requested temperature is out of range for '
                                 f'available partition function of {self.name}')
        except ValueError as e:
            print(e)

        return (griddata(Tgas_partition_function,
                         self.internal_partition_df['internal_partition'].values,
                         Tgas))

    def total_partition(self, Tgas):
        "Return total partition function."
        # use tabulated internal partition function if available
        if self.internal_partition_df.empty:
            return (self.translational_partition(Tgas)
                    * self.electronic_partition(Tgas))
        else:
            return (self.translational_partition(Tgas)
                    * self.internal_partition(Tgas))

    # def translational_entropy(self, Tgas):
    #     """Calculate the translational entropy.
    #
    #     At a pressure of 1 bar.
    #     """
    #     return k_B*(np.log(self.translational_partition(Tgas)) + 3./2.)
    #
    # def electronic_entropy(self, Tgas):
    #  #    """Calculate electronic entropy.
    #  #
    #  #    Under the assumption that the first electronic excitation energy
    #  #    is much greater than k_B*Tgas
    #  #    """
    #     return k_B * np.log(self.electronic_partition(Tgas))
    #
    # def total_entropy_1_particle(self, Tgas):
    #     return self.translational_entropy(Tgas) + self.electronic_entropy(Tgas)
    #
    #
    # def total_entropy_N_particles(self, Tgas, N=N_A.value):
    #     return (N*self.total_entropy_1(Tgas)
    #             - N*k_B*np.log(N)
    #             + N*k_B)
    #
    # def translational_energy(self, Tgas):
    #     """Calculate translational internal energy."""
    #     return 3./2. * k_B * Tgas
    #
    # def total_energy(self, Tgas):
    #     """Calculate total internal energy."""
    #     return (self.translational_energy(Tgas))
    #
    # def total_enthalpy(self, Tgas):
    #     """Calculate total total enthalpy."""
    #     return self.total_energy(Tgas) + k_B * Tgas

    def gibbs_free_energy_one_particle(self, Tgas):
        "Return total gibbs free energy valid for one paricle"
        return (self.energy + k_B * Tgas
                - k_B * Tgas * np.log(self.total_partition(Tgas))
                )

    def gibbs_free_energy_largeN_particles(self, Tgas, N=N_A.value):
        """Return total gibbs free energy valid forN particles.

        Note that this uses Stirling's approximation and is therefore
        only valid for N >> 1.
        Default is 1 mole of paricles.
        """
        if N < 1e6:
            print("N is too small to use the Gibbs free energy approximation"
                  " for large numbers")
            return

        else:
            return (N * self.energy
                    - N * k_B * Tgas * np.log(self.total_partition(Tgas))
                    + N * k_B * Tgas * np.log(N)
                    )


class Atom(Species):
    """Define an atom"""
    pass


class Molecule(Species):
    """Define a chemical molecule."""

    def __init__(self, name, mass, energy, elec_energies, elec_degeneracies,
                 internal_partition_df: pd.DataFrame, rot_temperatures,
                 rot_symmetry, vib_temperatures):
        """Initialise the molecule object."""
        super().__init__(name, mass, energy, elec_energies, elec_degeneracies,
                         internal_partition_df)
        self.rot_temperatures = rot_temperatures
        self.rot_symmetry = self.get_rot_symmetry(rot_symmetry)
        self.vib_temperatures = vib_temperatures

    @property
    def multiplied_rot_temp(self):
        """Multiply rotational temperatures."""
        result = 1

        for value in self.rot_temperatures:
            result *= value
        return result

    @staticmethod
    def get_rot_symmetry(rot_symmetry):
        if isinstance(rot_symmetry, list):
            rot_symmetry = rot_symmetry[0]

        return rot_symmetry

    def vibrational_partition(self, Tgas):
        """Calculate vibrational partition function."""
        result = 1

        for v_temp in self.vib_temperatures:
            x = v_temp / Tgas
            result *= np.exp(-0.5 * x) / (1 - np.exp(-x))
        return result

    def rotational_partition(self, Tgas):
        """Calculate rotational partition function."""
        return ((pi * Tgas**3 / self.multiplied_rot_temp)**(0.5)
                / self.rot_symmetry)

    def total_partition(self, Tgas):
        "Return total partition function."

        # use tabulated internal partition function if available
        if self.internal_partition_df.empty:
            return (self.translational_partition(Tgas)
                    * self.rotational_partition(Tgas)
                    * self.vibrational_partition(Tgas)
                    * self.electronic_partition(Tgas))
        else:
            return (self.translational_partition(Tgas)
                    * self.internal_partition(Tgas))

    # TODO: update everything below to make it compatible with new class input
    # def rotational_entropy(self, Tgas):
    #     """Calculate the translational entropy."""
    #     return R * (np.log(self.rotational_partition(Tgas)) + 3./2.)
    #
    # def vibrational_entropy(self, Tgas):
    #     """Calculate the translational entropy."""
    #     result = 0
    #
    #     for v_temp in self.vib_temperatures:
    #         x = v_temp / Tgas
    #         result += x / (np.exp(x) - 1.) - np.log(1. - np.exp(-x))
    #
    #     return R * result
    #
    # def std_translation_entropy(self, Tgas):
    #     """Calculate the translational entropy.
    #
    #     At a pressure of 1 bar.
    #     """
    #     return R*(np.log(self.std_translational_partition(Tgas)) + 1. + 3./2.)
    #
    # def translation_entropy(self, Tgas, pressure):
    #     """Calculate the translational entropy.
    #
    #     At any given pressure.
    #     """
    #     return (self.std_translation_entropy(Tgas)
    #             + R * np.log(u.bar / pressure))
    #
    # def electronic_entropy(self, Tgas):
    #     """Calculate electronic entropy.
    #
    #     Under the assumption that the first electronic excitation energy
    #     is much greater than k_B*Tgas
    #     """
    #     if (len(self.electron_energy_levels) > 1 or
    #             len(self.electron_spin_degens) > 1):
    #         print('''Electronic entropy is not defined for multiple
    #                 energy levels''')
    #         return
    #     else:
    #         return R * np.log(self.electron_spin_degens[0])
    #
    # def std_total_entropy(self, Tgas):
    #     """Calculate the total entropy of the species at 1 bar."""
    #     return (self.std_translation_entropy(Tgas)
    #             + self.rotational_entropy(Tgas)
    #             + self.vibrational_entropy(Tgas)
    #             + self.electronic_entropy(Tgas))
    #
    # def total_entropy(self, Tgas, pressure):
    #     """Calculate the total entropy of the species."""
    #     return (self.translation_entropy(Tgas, pressure)
    #             + self.rotational_entropy(Tgas)
    #             + self.vibrational_entropy(Tgas)
    #             + self.electronic_entropy(Tgas))
    #
    # def translational_energy(self, Tgas):
    #     """Calculate translational internal energy."""
    #     return 3./2. * R * Tgas
    #
    # def rotational_energy(self, Tgas):
    #     """Calculate rotational internal energy."""
    #     return 3./2. * R * Tgas
    #
    # def vibrational_energy(self, Tgas):
    #     """Calculate vibration internal energy."""
    #     result = 0
    #
    #     for v_temp in self.vib_temperatures:
    #         try:
    #             result += v_temp * (0.5 + 1./(np.exp(v_temp / Tgas) - 1))
    #         # Exception when division by zero when Tgas is zero.
    #         except(RuntimeWarning):
    #             result += v_temp * 0.5
    #
    #     return R * result
    #
    # def electronic_energy(self, Tgas):
    #     """Calculate electronic energy.
    #
    #     Under the assumption that the first electronic excitation energy
    #     is much greater than k_B*Tgas,
    #     this contribution is zero.
    #     """
    #     # if (len(self.elec_energies) > 1 or
    #     #         len(self.elec_degeneracies) > 1):
    #     #     print('''Electronic energy is not defined for multiple
    #     #             energy levels''')
    #     #     return
    #     # else:
    #     return 0.*u.J/u.mol
    #
    # def total_energy(self, Tgas):
    #     """Calculate total internal energy."""
    #     return (self.translational_energy(Tgas)
    #             + self.rotational_energy(Tgas)
    #             + self.vibrational_energy(Tgas)
    #             + self.electronic_energy(Tgas))
    #
    # def total_enthalpy(self, Tgas):
    #     """Calculate total total enthalpy."""
    #     return self.total_energy(Tgas) + R * Tgas
    #
    # def std_total_gibbs_free_energy(self, Tgas):
    #     """Calculate Gibbs free energy at 1 bar."""
    #     # return (self.total_enthalpy(Tgas)
    #     #         + self.energy
    #     #         - Tgas * self.std_total_entropy(Tgas)).to(u.kJ/u.mol)
    #
    #     return (R * Tgas - R * Tgas * np.log(self.total_partition(Tgas))
    #             + self.energy).to(u.kJ/u.mol)
    #
    # def total_gibbs_free_energy(self, Tgas, pressure):
    #     """Calculate Gibbs free energy."""
    #     return (self.total_enthalpy(Tgas)
    #             + self.energy
    #             - Tgas * self.total_entropy(Tgas, pressure))


class LinearMolecule(Molecule):
    """Define a linear molecule."""

    def rotational_partition(self, Tgas):
        """Calculate rotational partition function."""
        return Tgas / (self.rot_symmetry * self.rot_temperatures[0])

    def rotational_entropy(self, Tgas):
        """Calculate the translational entropy."""
        return R * (np.log(self.rotational_partition(Tgas)) + 1.)

    def rotational_energy(self, Tgas):
        """Calculate rotational internal energy."""
        return R * Tgas
