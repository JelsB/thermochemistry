import astropy.units as u
from astropy.constants import h, k_B, R, N_A
from pathlib import Path
import regex_patterns as rxp
import functools


def freq_to_temp(freqs):
    """Convert fequency to temperature in K.

    Input freqs is in cm^-1
    """
    return freqs.to(1. / u.s, equivalencies=u.spectral()) * h / k_B


def freq_to_energy(freqs):
    """Convert fequency to energy in J.

    Input freqs is in cm^-1
    """
    return freqs.to(1. / u.s, equivalencies=u.spectral()) * h


def path_molecule_filter(path: Path, patern=rxp.molecule_patern):
    """Filter path objects for molecule patern files."""
    return patern.match(path.name)


def path_atom_filter(path: Path, patern=rxp.atom_patern):
    """Filter path objects for atom patern files."""
    return patern.match(path.name)


def all_subclasses(cls):
    return set(cls.__subclasses__()).union(
        [sub for cl in cls.__subclasses__() for sub in all_subclasses(cl)])


def cleaned_to_cgs(func):
    """
    decomposes the units and converts them to cgs per particle
    instead of per mole
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs).decompose().cgs
        if 'mol' in str(result.unit):
            result = result / N_A
        return result
    return wrapper
