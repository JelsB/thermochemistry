from pathlib import Path
import re
import pandas as pd
import csv

input_file = Path('data/literature_input/HITRAN_input.pf')
output_path = Path('data/literature_input/')
wanted_species = ['HO2', 'H2O2', 'O2', 'N2', 'N2O', 'SO2', 'NO2', 'SO']
max_name_len = 10  # lenght to recognise molecules


def clean_raw_data(source_iter):
    species = []
    data = []
    data_species = []
    header = []

    for row in source_iter:
        # skip comments
        if row[0] == 'c':
            continue
        # if new species
        if len(row.strip()) < max_name_len:
            # append all species data if available
            if data_species:
                data.append(data_species)
                data_species = []
            # append species and header data
            species.append(row.strip())
            cleaned_header = clean_header(next(source_iter))
            num_of_columns = len(cleaned_header)
            header.append(cleaned_header)
        else:
            data_species.append(clean_data(row, num_of_columns))

    # append last species data
    data.append(data_species)

    yield species
    yield header
    yield data


def clean_header(header):
    return list(map(str.strip, header.split('Q(T)')))


def clean_data(data, max_column):
    return list(
        filter(None, map(str.strip, data.strip().split('  ')))
    )[:max_column]


def write_csv(path: Path, columns: iter, header):
    with output_file.open('w') as out:
        out_writer = csv.writer(out, delimiter=',')
        out_writer.writerow(header)
        out_writer.writerows(columns)


# read in and clean raw input file
with input_file.open('r') as f:
    species, headers, data = list(clean_raw_data(f))

# write csv output files
cnt = 0
for sp in species:
    output_file = output_path.joinpath(sp + '_input').with_suffix('.pf')
    #  write only when file not yet exists (maybe from other source)
    #  and if it is a desired molecule
    if not output_file.exists() and sp in wanted_species:
        write_csv(output_file, data[cnt], headers[cnt])
    cnt += 1
