import re
import pandas as pd
import astropy.units as u
import regex_patterns as rxp
import parse_files as parf
from pathlib import Path
from collections import defaultdict
from utils import freq_to_temp, freq_to_energy
from atomic_info import count_constituent_atoms, is_atom
from inout import (get_input_files, objs_to_json, json_to_objs,
                   write_out_gibbs_free_energy)
from chemical_species import Species, Atom, Molecule, LinearMolecule
from literatur_chemical_species import (LiteratureSpecies, LiteratureAtom,
                                        LiteratureMolecule)


def list_to_float(list):
    return [float(i) for i in list]


def list_of_list_to_float(list_of_list):
    return [list_to_float(i) for i in list_of_list]


def make_species(name, filenames):
    literature_data = []
    gaussian_data = []
    thermochem_data = None
    literature_species = None
    rot_temperatures = []
    partition_data = pd.DataFrame()
    use_literature_energies = False
    species_mass = None

    atom_counter = count_constituent_atoms(name)

    for filename in filenames:
        extension = filename.suffix
        with filename.open('r') as f:
            if extension == '.dat':
                literature_data = parf.get_species_data(f)

            elif extension == '.dft':
                gaussian_data = list_of_list_to_float(
                    list(parf.parse_gaussian_data(f)))

            elif extension == '.pf':
                partition_data = parf.get_internal_partition_function(f)

            elif extension == '.therm':
                thermochem_data = parf.get_thermochem_data(f, name)

            else:
                print('{} is not yet usuable.'.format(f.name))

    if gaussian_data and not partition_data.empty:
        print('Gaussian and literature parition function data should '
              'not be mixed')

    if literature_data:
        (energy, vib_frequencies, rot_frequencies, rot_symmetry,
         lit_elec_frequencies, lit_elec_degeneracy) = literature_data

        vib_temperatures = freq_to_temp(vib_frequencies / u.cm)
        rot_temperatures = freq_to_temp(rot_frequencies / u.cm)
        lit_elec_energies = freq_to_energy(lit_elec_frequencies / u.cm)

        elec_degeneracies = lit_elec_degeneracy
        elec_energies = lit_elec_energies

        if len(lit_elec_energies) > 1:
            use_literature_energies = True

    if gaussian_data:
        (energy, rot_symmetry, vib_frequencies, rot_temperatures,
         gaussian_elec_degeneracy, species_mass) = gaussian_data

        # convert one value list to value
        energy = energy[0]
        species_mass = species_mass[0]
        # convert to quantity objects with units
        rot_temperatures *= u.K
        vib_temperatures = freq_to_temp(vib_frequencies / u.cm)
        species_mass *= u.u  # atomic mass

        elec_energies = [0] * u.J
        elec_degeneracies = gaussian_elec_degeneracy

        if use_literature_energies:
            elec_degeneracies = lit_elec_degeneracy
            elec_energies = lit_elec_energies

    # make literature species objects
    if thermochem_data:
        if is_atom(name):
            diff_enthalpy, energy_therm = thermochem_data
            literature_species = LiteratureAtom(name, diff_enthalpy, energy)

        else:
            (temperature, entropy, diff_enthalpy, formation_enthalpy_0,
             formation_enthalpy_298, energy_therm, E_zpve) = thermochem_data

            literature_species = LiteratureMolecule(name, temperature, entropy,
                                                    diff_enthalpy,
                                                    formation_enthalpy_0,
                                                    formation_enthalpy_298,
                                                    energy, E_zpve)
        # return species

    # make species objects
    if len(rot_temperatures) > 1:
        species = Molecule(name, species_mass, energy, elec_energies,
                           elec_degeneracies, partition_data,
                           rot_temperatures, rot_symmetry,
                           vib_temperatures)

    elif len(rot_temperatures) == 1:
        species = LinearMolecule(name, species_mass, energy, elec_energies,
                                 elec_degeneracies, partition_data,
                                 rot_temperatures, rot_symmetry,
                                 vib_temperatures)

    elif is_atom(name):
        species = Atom(name, species_mass, energy, elec_energies,
                       elec_degeneracies, partition_data)

    elif not thermochem_data:
        print('Something went wrong when making species {}'.format(name))

    return species, literature_species


def make_species_from_files(species_data_files, include_literature=False):
    species_file_dict = defaultdict(list)
    dict_of_species = {}
    dict_of_lit_atom_mol = {}

    for sp_file in species_data_files:
        name = re.match(rxp.species_pattern, sp_file.stem).group()
        species_file_dict[name].append(sp_file)

    for sp, files in species_file_dict.items():
        species, literature_species = make_species(sp, files)
        print(species, literature_species)
        if species:
            dict_of_species[sp] = species
        if literature_species:
            dict_of_lit_atom_mol[sp] = literature_species

    if not include_literature:
        return dict_of_species
    else:
        return dict_of_species, dict_of_lit_atom_mol


if __name__ == '__main__':

    # Create objects from files or JSON
    # Files (with new data)
    input_files = get_input_files()
    dict_of_species = make_species_from_files(input_files)

    # JSON (with existing data)
    # dict_of_species = json_to_objs(Path('./output/thermo_all.json'))

    # define temperatures and pressure of interest
    gas_temperature = list(range(100, 3010, 10)) * u.K
    # empty = default = 1 bar
    gas_pressure = []

    # write out Gibbs free energy csv files for each species
    for species in dict_of_species.values():
        write_out_gibbs_free_energy(species, gas_temperature, gas_pressure)

    # # write objects to json file (to use later or easily export)
    objs_to_json(dict_of_species, Path('./output/thermo_all.json'),
                 skip_key=None)
