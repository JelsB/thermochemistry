import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path


# TODO: clean this function
def write_csv_from_table_file():
    # species_list = ['HO2', 'H2O2', 'O2', 'N2', 'N2O', 'SO2', 'NO2', 'SO']
    species_list = ['H2']
    for species in species_list:
        input_file = Path('data/literature_input/{}_input.pf'.format(species))
        output_file = Path('data/{}.pf'.format(species))
        select_colums = [0, 1]
        # header_key = 0  # 0: first row is header, None: no header
        header_key = None
        new_header = ['T (K)', 'Z']
        # select_colums = ['T / K', 'Q(T)   161']
        # rename_colums = {select_colums[0]: 'T (K)', select_colums[1]: 'Z'}
        # rename_colums = dict()
        # Read in file, and make data frame
        # df_in = pd.read_csv(input_file, delimiter=',', delim_whitespace=False,
        # header=header_key)
        df_in = pd.read_csv(input_file, delim_whitespace=True, header=header_key)
        # select wanted colums
        # df_out = df_in.loc[:, select_colums]  # by label
        df_out = df_in.iloc[:, select_colums]  # by index

        # rename column headers
        header = list(df_out)
        rename_colums = {old: new for (old, new) in zip(header, new_header)}
        df_out.rename(columns=rename_colums, inplace=True)

        # remove zero values
        df_out2 = df_out[df_out['Z'] != 0]

        # write out as csv if not yet exists
        if not output_file.exists():
            df_out2.to_csv(output_file, index=False)
            print("Wrote csv file for {}".format(species))


def rename_dataframe_header(dataframe, new_header):
    input_header = list(dataframe)
    rename_header = {old: new for (old, new) in zip(input_header, new_header)}
    return dataframe.rename(columns=rename_header, inplace=True)
