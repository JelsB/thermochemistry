import re

# RegEx paterns
# patern of XnYmZk... and Xn type of molecules
molecule_patern = re.compile(r'([A-Z]+[a-z]*\d*){2,}|[A-Z]+\d+')
atom_patern = re.compile(r'[A-Z]+[a-z]*')
species_pattern = re.compile(molecule_patern.pattern + '|' + atom_patern.pattern)
