import glob
import json
import csv
import astropy.units as u
from pathlib import Path
from argparse import Namespace

from astropy.utils.misc import JsonCustomEncoder
from chemical_species import Atom, Molecule, LinearMolecule
from atomic_info import is_atom
from reaction import (Reaction, Arrhenius, ModifiedArrhenius, Lindemann, Troe,
                      RigidSpheres)


# needed directories
options = Namespace(input_path=Path('./input'),
                    output_path=Path('./output'),
                    overwrite_files=False
                    )


def get_input_files():
    gaussian_data_files = list(options.input_path.glob("*.dft"))
    literature_species_data_files = list(options.input_path.glob("*.dat"))
    partition_function_files = list(options.input_path.glob("*.pf"))
    thermochemical_files = list(options.input_path.glob("*.therm"))
    species_data_files = (literature_species_data_files + gaussian_data_files
                          + partition_function_files + thermochemical_files)

    return species_data_files


def write_out_gibbs_free_energy(species, gas_temperature, pressure,
                                out_path=options.output_path):

    krome_name = species.name.upper()
    file_suffix = '.gfe'
    gibbs_free_energy = species.gibbs_free_energy_largeN_particles(gas_temperature).to(u.kJ) / u.mol
    out_file = out_path.joinpath(krome_name).with_suffix(file_suffix)
    out_header = ['temperature ({})'.format(gas_temperature.unit),
                  'Gibbs free energy ({})'.format(
        gibbs_free_energy.unit)]

    out_columns = zip(gas_temperature.value, gibbs_free_energy.value)

    if not options.overwrite_files and out_file.exists():
        print(f'File {out_file} already exists.')
        return

    write_csv(out_file, out_columns, out_header)
    return


def objs_to_json(dict_of_objs, output_file, skip_key=None):
    """Dumps dict of objects to json file.
    Args:
        output_file (Pathlib obj): output file
        skip_key (str): key word that is skipped for output
    """
    json_dict = dict()
    for key, species in dict_of_objs.items():
        # convert object to dict
        attrs = species.convert_to_dict(skip_attrs=skip_key)
        json_dict[species.name] = attrs

    with output_file.open('w') as f:
        # use custom Astropy Json encoder
        # e.g. to handle Quantity objects
        json.dump(json_dict, fp=f, cls=JsonCustomEncoder, indent=2)


def json_to_objs(json_file):
    dict_of_objs = dict()

    with json_file.open('r') as f:
        data_dict = json.load(f)
    print('Done reading in')

    for key, item in data_dict.items():
        if is_atom(key):
            sp = Atom.from_dict(item)
        elif len(item['rot_temperatures']['value']) == 1:
            sp = LinearMolecule.from_dict(item)
        else:
            sp = Molecule.from_dict(item)

        dict_of_objs[key] = sp
        print(sp)
    return dict_of_objs


def write_csv(output_file, columns, header, delimiter=' '):
    # output_file = path.with_suffix(suffix)

    with output_file.open('w') as out:
        out_writer = csv.writer(out, delimiter=delimiter, quotechar='#')
        out_writer.writerow(header)
        out_writer.writerows(columns)
    print('Wrote out {}'.format(output_file.name))


def reactions_from_json(json_file, dict_of_species):
    dict_of_objs = dict()

    with json_file.open('r') as f:
        reactions_dict = json.load(f)

    for key, value in reactions_dict.items():
        react = Reaction.from_dict(value, dict_of_species)
        dict_of_objs[key] = react
        print(react)
    return dict_of_objs
