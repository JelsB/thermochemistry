import numpy as np
import astropy.units as u
from astropy.constants import h, k_B, R, N_A
from scipy.interpolate import griddata
from collections import Counter
from typing import List
from chemical_species import LinearMolecule


Hartree = (2. * u.Ry).to(u.J)


class LiteratureAtom:
    """Define an atom.

    All input data comes from themochemical tables that do not provide
    fundamental degrees of freedom (rotational constants, vibrational
    frequencies) or partition functions. This data can however be used
    to reverse engineer desired properties like Gibbe free energy.

    NOTE that all functions/attributes are at reference value of 0.1 MPa
    """

    def __init__(self, name, diff_enthalpy, energy):
        self.name = name
        self.diff_enthalpy = diff_enthalpy * u.kJ / u.mol
        self.energy = (energy * Hartree * N_A).to(u.kJ / u.mol)

    def __repr__(self):
        return "{class_}({name})".format(
            class_=self.__class__.__name__, **vars(self))


class LiteratureMolecule:
    """Define a molecule.

    All input data comes from themochemical tables that do not provide
    fundamental degrees of freedom (rotational constants, vibrational
    frequencies) or partition functions. This data can however be used
    to reverse engineer desired properties like Gibbe free energy.

    NOTE that all functions/attributes are at reference value of 0.1 MPa
    """

    def __init__(self, name, temperature, entropy, diff_enthalpy,
                 formation_enthalpy_0, formation_enthalpy_298, energy, E_zpve):
        """Initialise the atom object."""
        self.name = name
        self.raw_temperature = np.array(temperature) * u.K
        self.raw_entropy = np.array(entropy)
        self.raw_diff_enthalpy = np.array(diff_enthalpy)
        self.ref_formation_enthalpy_0 = formation_enthalpy_0 * u.kJ / u.mol
        self.ref_formation_enthalpy_298 = formation_enthalpy_298 * u.kJ / u.mol
        self.zero_point_vibrational_energy = (E_zpve * N_A * u.eV).to(u.kJ / u.mol)
        self.energy = (energy * Hartree * N_A).to(u.kJ / u.mol)

    def __repr__(self):
        return "{class_}({name})".format(
            class_=self.__class__.__name__, **vars(self))

    def total_entropy(self, Tgas):
        "Interpolate entropy from given list."
        return (griddata(self.raw_temperature, self.raw_entropy, Tgas)
                * u.J / (u.K * u.mol))

    def diff_enthalpy(self, Tgas):
        "Interpolate enthalpy difference (wrt ref) from given list."
        # print(self.raw_diff_enthalpy)
        return griddata(self.raw_temperature,
                        self.raw_diff_enthalpy, Tgas) * u.kJ / u.mol


class LiteratureSpecies:
    """Define a molecule ans its atomic constituents."""

    def __init__(self, molecule: LinearMolecule, atoms: List[LiteratureAtom],
                 counter: Counter):
        self.molecule = molecule
        self.atoms = atoms
        self.atom_counter = counter

    @property
    def enthalpy_298(self):
        atomic_part = 0
        for atm in self.atoms:
            atomic_part += self.atom_counter[atm.name] * atm.diff_enthalpy

        return (self.molecule.ref_formation_enthalpy_298
                - self.molecule.ref_formation_enthalpy_0
                + self.molecule.zero_point_vibrational_energy
                + atomic_part
                )

    def gibbs_free_energy(self, Tgas):
        return (self.molecule.diff_enthalpy(Tgas)
                + self.enthalpy_298
                + self.molecule.energy
                - Tgas * self.molecule.total_entropy(Tgas)
                ).to(u.J / u.mol)
